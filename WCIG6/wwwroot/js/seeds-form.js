﻿$(document).ready(function () {
    var $form = $("form");
    var $isPublishedInput = $("#isPublishedInput");
    var $publishDialog = $("#publishDialog");

    $("#registerButton").on("click", function () {
        // Only showing dialog when a new seed categroy is being created
        if (isNew) {
            $publishDialog.modal('show');
        } else {
            $form.submit();
        }
    });

    $("#publishButton").on("click", function () {
        $isPublishedInput.val(true);

        $form.submit();
    });

    $("#saveButton").on("click", function () {
        $isPublishedInput.val(false);

        $form.submit();
    });
});
