﻿function submitFormWithId($form, seedId) {
    $form.find("input[name=seedId]").val(seedId);

    $form.submit();
}

$(document).ready(function () {
    var currentSeedId = null;

    var $deleteForm = $("#deleteForm");
    var $publishForm = $("#publishForm");
    var $unpublishForm = $("#unpublishForm");

    var $deleteDialog = $("#deleteDialog");

    $("button[data-delete]").on("click", function (event) {
        currentSeedId = event.target.dataset["seed"];

        if (Number(event.target.dataset["delete"])) {
            // Not published -> Deleting right away
            submitFormWithId($deleteForm, currentSeedId);
        } else {
            // Published -> Prompting to unpublish
            $deleteDialog.modal('show');
        }
    });

    $("button[data-publish]").on("click", function (event) {
        submitFormWithId($publishForm, event.target.dataset["seed"]);
    });

    $("button[data-unpublish]").on("click", function (event) {
        submitFormWithId($unpublishForm, event.target.dataset["seed"]);
    });

    $("#unpublishButton").on("click", function (event) {
        if (currentSeedId != null) {
            submitFormWithId($unpublishForm, currentSeedId);
        }
    });

    $("#deleteButton").on("click", function (event) {
        if (currentSeedId != null) {
            submitFormWithId($deleteForm, currentSeedId);
        }
    });
});
