﻿using System;
namespace WCIG6.Business.VendorVerification
{
    public class VendorVerificationCode
    {
        private static readonly int CODE_LENGTH = 6;
        private static readonly Random random = new Random();

        public CodeType Type { get; set; }

        public string Code { get; set; }

        public override string ToString()
        {
            return $"${Code}:${Type}";
        }

        public static VendorVerificationCode Generate(CodeType type)
        {
            string code = "";

            for(int i = 0; i < CODE_LENGTH; i++)
            {
                code += random.Next(0, 10).ToString();
            }

            return new VendorVerificationCode() { Code = code, Type = type };
        }

        public enum CodeType
        {
            Phone,
            Email
        }
    }
}
