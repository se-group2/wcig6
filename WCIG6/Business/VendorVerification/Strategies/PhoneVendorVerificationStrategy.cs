﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using WCIG6.Infrastructure.SMS;
using WCIG6.Models;

namespace WCIG6.Business.VendorVerification.Strategies
{
    public class PhoneVendorVerificationStrategy : VendorVerificationStrategy
    {
        private static readonly int MIN_PHONE_LENGTH = 10;
        private static readonly int MAX_PHONE_LENGTH = 12;

        private readonly DbSet<Vendor> vendorsRepo;
        private readonly SMSClient smsClient;

        private bool checkPhoneFormat(string phone)
        {
            if (phone == null)
            {
                return false;
            }

            string sanitizedPhone = Regex.Replace(phone, @"[^0-9]", "", RegexOptions.Multiline);
            int length = sanitizedPhone.Length;

            return length >= MIN_PHONE_LENGTH && length <= MAX_PHONE_LENGTH;
        }

        private bool checkPhoneUniqueness(string phone)
        {
            var foundVendor = vendorsRepo.SingleOrDefault(vendor => vendor.PhoneNumber == phone);

            return foundVendor == null;
        }

        public PhoneVendorVerificationStrategy(
            DbSet<Vendor> vendorsRepo,
            SMSClient smsClient
        ) {
            this.vendorsRepo = vendorsRepo;
            this.smsClient = smsClient;
        }

        public void verify(Vendor vendor)
        {
            if(!checkPhoneFormat(vendor.PhoneNumber))
            {
                throw new VendorVerficationError("Invalid phone number");
            }

            if (!checkPhoneUniqueness(vendor.PhoneNumber))
            {
                throw new VendorVerficationError("Phone number is already being used");
            }
        }

        public bool SupportsCode => true;

        // Implementation: Could be listening for response on a certain phone number
        public VendorVerificationCode sendCode(Vendor vendor)
        {
            var code = VendorVerificationCode.Generate(VendorVerificationCode.CodeType.Phone);

            smsClient.send(vendor.PhoneNumber, $"Verification code: ${code.Code}");

            return code;
        }
    }
}
