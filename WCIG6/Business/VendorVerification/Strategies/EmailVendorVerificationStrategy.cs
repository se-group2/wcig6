﻿using System;
using System.Linq;
using System.Net.Mail;
using Microsoft.EntityFrameworkCore;
using WCIG6.Infrastructure.Mail;
using WCIG6.Models;

namespace WCIG6.Business.VendorVerification.Strategies
{
    public class EmailVendorVerificationStrategy : VendorVerificationStrategy
    {
        private readonly DbSet<Vendor> vendorsRepo;
        private readonly MailClient mailClient;

        private bool checkEmailFormat(string email)
        {
            if (email == null)
            {
                return false;
            }

            try
            {
                var addr = new MailAddress(email);

                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private bool checkEmailUniqueness(string email)
        {
            var foundVendor = vendorsRepo.SingleOrDefault(vendor => vendor.Email == email);

            return foundVendor == null;
        }

        public EmailVendorVerificationStrategy(
            DbSet<Vendor> vendorsRepo,
            MailClient mailClient
        )
        {
            this.vendorsRepo = vendorsRepo;
            this.mailClient = mailClient;
        }

        public void verify(Vendor vendor)
        {
            if (!checkEmailFormat(vendor.Email))
            {
                throw new VendorVerficationError("Invalid email address");
            }

            if (!checkEmailUniqueness(vendor.Email))
            {
                throw new VendorVerficationError("Email address is already being used");
            }
        }

        public bool SupportsCode => true;

        // Implementation: Exposing a web-hook as a controller action
        public VendorVerificationCode sendCode(Vendor vendor)
        {
            var code = VendorVerificationCode.Generate(VendorVerificationCode.CodeType.Email);

            mailClient.send(vendor.Email, $"Verification code: ${code.Code}");

            return code;
        }
    }
}
