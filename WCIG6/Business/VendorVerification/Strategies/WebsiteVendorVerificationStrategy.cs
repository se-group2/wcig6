﻿using System;
using WCIG6.Infrastructure.Http;
using WCIG6.Models;

namespace WCIG6.Business.VendorVerification.Strategies
{
    public class WebsiteVendorVerificationStrategy : VendorVerificationStrategy
    {
        private readonly HttpClient httpClient;

        public WebsiteVendorVerificationStrategy(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public void verify(Vendor vendor)
        {
            if (string.IsNullOrEmpty(vendor.Website)) return;

            if (httpClient.testURL(vendor.Website)) return;

            throw new VendorVerficationError("Website is not reachable");
        }

        public bool SupportsCode => false;

        public VendorVerificationCode sendCode(Vendor vendor)
        {
            return null;
        } 
    }
}
