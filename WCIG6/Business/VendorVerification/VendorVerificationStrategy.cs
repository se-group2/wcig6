﻿using System;
using WCIG6.Models;

namespace WCIG6.Business.VendorVerification
{
    public interface VendorVerificationStrategy
    {
        void verify(Vendor vendor);

        bool SupportsCode { get; }
        VendorVerificationCode sendCode(Vendor vendor);
    }
}
