﻿using System;
namespace WCIG6.Business.VendorVerification
{
    [Serializable]
    public class VendorVerficationError : Exception
    {
        private readonly string message;

        public VendorVerficationError(string message) : base(message)
        {
            this.message = message;
        }

        public string VerificationMessage => message;
    }
}
