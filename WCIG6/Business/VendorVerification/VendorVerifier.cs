﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WCIG6.Models;

namespace WCIG6.Business.VendorVerification
{
    public class VendorVerifier
    {
        private readonly ICollection<VendorVerificationStrategy> strategies;
        private readonly Database database;

        private void saveVerificationCodes(ICollection<VendorVerificationCode> codes, Vendor vendor)
        {
            var verificationCodes = new List<VerificationCode>();

            foreach(var code in codes)
            {
                if(code != null)
                {
                    verificationCodes.Add(new VerificationCode() { Code = code.ToString(), Vendor = vendor });
                }
            }

            vendor.Codes = verificationCodes;

            database.Code.AddRange(verificationCodes);
        }

        public VendorVerifier(
            ICollection<VendorVerificationStrategy> strategies,
            Database database
        ) {
            this.strategies = strategies;
            this.database = database;
        }

        public VerificationResult verify(Vendor vendor)
        {
            var errors = new List<string>();

            foreach(var strategy in strategies)
            {
                try
                {
                    strategy.verify(vendor);
                }
                catch(VendorVerficationError error)
                {
                    errors.Add(error.VerificationMessage);
                }
            }

            if(errors.Count > 0)
            {
                return new VerificationResult()
                {
                    IsSuccessful = false,
                    Errors = errors,
                };
            }
            else
            {
                return new VerificationResult()
                {
                    IsSuccessful = true,
                    Errors = errors,
                };
            }
        }

        public void sendCodes(Vendor vendor)
        {
            var codes = new List<VendorVerificationCode>();

            foreach (var strategy in strategies)
            {
                if(strategy.SupportsCode)
                {
                    var code = strategy.sendCode(vendor);

                    if (code != null)
                    {
                        codes.Add(code);
                    }
                }
            }

            if (codes.Count > 0)
            {
                vendor.IsVerified = false;

                saveVerificationCodes(codes, vendor);
            }
            else
            {
                vendor.IsVerified = true;
            }

            database.Vendor.Update(vendor);

            database.SaveChanges();
        }

        public void submitCode(string verificationCode)
        {
            var code = database.Code
                .Include(code => code.Vendor)
                .ThenInclude(vendor => vendor.Codes)
                .SingleOrDefault(code => code.Code == verificationCode);

            if (code == null) return;

            var vendor = code.Vendor;

            // If this is the final code to verify -> Update vendor to be verified as well
            if(vendor.Codes.Count == 1)
            {
                vendor.IsVerified = true;

                database.Vendor.Update(vendor);
            }

            database.Code.Remove(code);

            database.SaveChanges();
        }

        public class VerificationResult
        {
            public bool IsSuccessful { get; set; }

            public ICollection<string> Errors { get; set; }
        }
    }
}
