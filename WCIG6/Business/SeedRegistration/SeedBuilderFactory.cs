﻿using System;
using Microsoft.EntityFrameworkCore;
using WCIG6.Models;

namespace WCIG6.Business.SeedRegistration
{
    public class SeedBuilderFactory
    {
        private readonly DbSet<Plant> plantsRepo;
        private readonly SeedBuilder.SeedDescriptionProvider descriptionProvider;
        private readonly SeedBuilder.SeedHardinessZoneSelector hardinessZoneSelector;
        private readonly SeedBuilder.SeedPriceConverter priceConverter;

        public SeedBuilderFactory(
            DbSet<Plant> plantsRepo,
            SeedBuilder.SeedDescriptionProvider descriptionProvider,
            SeedBuilder.SeedHardinessZoneSelector hardinessZoneSelector,
            SeedBuilder.SeedPriceConverter priceConverter
        ) {
            this.plantsRepo = plantsRepo;
            this.descriptionProvider = descriptionProvider;
            this.hardinessZoneSelector = hardinessZoneSelector;
            this.priceConverter = priceConverter;
        }

        public SeedBuilder forPlant(int plantId)
        {
            var selectedPlant = plantsRepo.Find(plantId);

            return new SeedBuilder(
                descriptionProvider,
                hardinessZoneSelector,
                priceConverter,
                selectedPlant
            );
        }
    }
}
