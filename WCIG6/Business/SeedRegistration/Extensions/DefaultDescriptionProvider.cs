﻿using System;
using WCIG6.Models;

namespace WCIG6.Business.SeedRegistration.Extensions
{
    public class DefaultDescriptionProvider : SeedBuilder.SeedDescriptionProvider
    {
        public string descriptionFrom(string seedDescription, Plant plant)
        {
            if (string.IsNullOrEmpty(seedDescription))
            {
                return $"Seeds of a \"{plant.Name}\"";
            }

            return seedDescription;
        }
    }
}
