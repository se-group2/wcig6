﻿using System;
namespace WCIG6.Business.SeedRegistration.Extensions
{
    public class SimplePriceConverter : SeedBuilder.SeedPriceConverter
    {
        // Assumption: The provided price is non-negative
        public int pricePerKGFrom(double price)
        {
            return (int)Math.Floor(price * 100);
        }
    }
}
