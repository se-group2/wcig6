﻿using System;
using WCIG6.Models;

namespace WCIG6.Business.SeedRegistration.Extensions
{
    public class LowestHardinessZoneSelector : SeedBuilder.SeedHardinessZoneSelector
    {
        public int hardinessZoneFrom(int? hardinessZone, Plant plant)
        {
            if (hardinessZone == null)
            {
                return plant.HardinessZone;
            }

            return Math.Min((int) hardinessZone, plant.HardinessZone);
        }
    }
}
