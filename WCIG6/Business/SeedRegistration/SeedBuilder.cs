﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using WCIG6.Models;

namespace WCIG6.Business.SeedRegistration
{
    public class SeedBuilder
    {
        private static readonly int MIN_PRICE = 1;
        private static readonly int MAX_PRICE = 500;
        private static readonly int MAX_HARDINESS_ZONE = 13;

        private readonly SeedDescriptionProvider descriptionProvider;
        private readonly SeedHardinessZoneSelector hardinessZoneSelector;
        private readonly SeedPriceConverter priceConverter;

        private readonly Plant selectedPlant;
        private Vendor seedVendor;
        private string seedName;
        private double seedPrice;
        private int? chosenHardinessZone;
        private string seedDescription;
        private bool seedVisibility;

        private ICollection<string> validateSeedInformation()
        {
            var errors = new List<string>();

            if(selectedPlant == null)
            {
                errors.Add("Plant not selected");
            }

            if(seedVendor == null)
            {
                errors.Add("Vendor not specified");
            }

            if (seedName == null)
            {
                errors.Add("Name not given");
            }

            if (seedPrice == -1)
            {
                errors.Add("The price is not provided");
            }
            else if (seedPrice <= 0)
            {
                errors.Add("The price is invalid");
            }
            else if(seedPrice < MIN_PRICE)
            {
                errors.Add("The price is too low");
            }
            else if (seedPrice > MAX_PRICE)
            {
                errors.Add("The price is too high");
            }

            if(chosenHardinessZone > MAX_HARDINESS_ZONE)
            {
                errors.Add("Invalid hardiness zone");
            }

            return errors;
        }

        public SeedBuilder(
            SeedDescriptionProvider descriptionProvider,
            SeedHardinessZoneSelector hardinessZoneSelector,
            SeedPriceConverter priceConverter,
            Plant plant
        ) {
            this.descriptionProvider = descriptionProvider;
            this.hardinessZoneSelector = hardinessZoneSelector;
            this.priceConverter = priceConverter;

            selectedPlant = plant;
            seedName = null;
            seedPrice = -1;
            chosenHardinessZone = null;
            seedDescription = null;
            seedVisibility = false;
        }

        public SeedBuilder withVendor(Vendor vendor)
        {
            seedVendor = vendor;

            return this;
        }

        public SeedBuilder withName(string name)
        {
            seedName = name;

            return this;
        }

        public SeedBuilder withPrice(double price)
        {
            seedPrice = price;

            return this;
        }

        public SeedBuilder withHardinessZone(int hardinessZone)
        {
            chosenHardinessZone = hardinessZone;

            return this;
        }

        public SeedBuilder withDescription(string description)
        {
            seedDescription = description;

            return this;
        }

        public SeedBuilder withVisibility(bool visibility)
        {
            seedVisibility = visibility;

            return this;
        }

        public Result build(int? seedId = null)
        {
            var errors = validateSeedInformation();

            if(errors.Count > 0)
            {
                return new Result
                {
                    IsSuccessful = false,
                    Errors = errors,
                };
            }

            try
            {
                var seed = new Seed
                {
                    Plant = selectedPlant,
                    Vendor = seedVendor,
                    Name = seedName,
                    PricePerKG = priceConverter.pricePerKGFrom(seedPrice),
                    HardinessZone = hardinessZoneSelector.hardinessZoneFrom(chosenHardinessZone, selectedPlant),
                    Description = descriptionProvider.descriptionFrom(seedDescription, selectedPlant),
                    IsVisible = seedVisibility,
                };

                if(seedId != null)
                {
                    seed.SeedId = (int) seedId;
                }

                return new Result
                {
                    IsSuccessful = true,
                    Seed = seed,
                };
            }
            catch(Exception error)
            {
                return new Result
                {
                    IsSuccessful = false,
                    Errors = new List<string>() { error.Message },
                };
            }
        }

        public class Result
        {
            public bool IsSuccessful { get; set; }

            public Seed Seed { get; set; }

            public ICollection<string> Errors { get; set; }
        }

        public interface SeedPriceConverter
        {
            int pricePerKGFrom(double price);
        }

        public interface SeedHardinessZoneSelector
        {
            int hardinessZoneFrom(int? hardinessZone, Plant plant);
        }

        public interface SeedDescriptionProvider
        {
            string descriptionFrom(string seedDescription, Plant plant);
        }
    }
}
