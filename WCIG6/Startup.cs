using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using WCIG6.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WCIG6.Infrastructure.Http;
using WCIG6.Infrastructure.Mail;
using WCIG6.Infrastructure.SMS;
using WCIG6.Business.VendorVerification;
using WCIG6.Business.VendorVerification.Strategies;
using WCIG6.Business.SeedRegistration;
using WCIG6.Business.SeedRegistration.Extensions;

namespace WCIG6
{
    public class Startup
    {
        private void ConfigureDatabase(IServiceCollection services)
        {
            services.AddDbContext<Database>(options =>
                options.UseSqlite(
                    Configuration.GetConnectionString("DefaultConnection")));
        }

        private void ConfigureClients(IServiceCollection services)
        {
            services.AddHttpClient();

            services.AddSingleton<HttpClient, SyncHttpClient>();
            services.AddSingleton<MailClient, SmtpMailClient>();
            services.AddSingleton<SMSClient, TwilioSMSClient>();
        }

        private void ConfigureVendorVerification(IServiceCollection services)
        {
            services.AddScoped<VendorVerifier, VendorVerifier>(sp =>
            {
                var httpClient = sp.GetRequiredService<HttpClient>();
                var mailClient = sp.GetRequiredService<MailClient>();
                var smsClient = sp.GetRequiredService<SMSClient>();

                var database = sp.GetRequiredService<Database>();

                var strategies = new List<VendorVerificationStrategy>();
                strategies.Add(new WebsiteVendorVerificationStrategy(httpClient));
                strategies.Add(new PhoneVendorVerificationStrategy(database.Vendor, smsClient));
                strategies.Add(new EmailVendorVerificationStrategy(database.Vendor, mailClient));

                return new VendorVerifier(strategies, database);
            });
        }

        private void ConfigureSeedRegistration(IServiceCollection services)
        {
            services.AddSingleton<SeedBuilder.SeedDescriptionProvider, DefaultDescriptionProvider>();
            services.AddSingleton<SeedBuilder.SeedHardinessZoneSelector, LowestHardinessZoneSelector>();
            services.AddSingleton<SeedBuilder.SeedPriceConverter, SimplePriceConverter>();

            services.AddScoped<SeedBuilderFactory, SeedBuilderFactory>(sp =>
            {
                var descriptionProvider = sp.GetRequiredService<SeedBuilder.SeedDescriptionProvider>();
                var hardinessZoneSelector = sp.GetRequiredService<SeedBuilder.SeedHardinessZoneSelector>();
                var priceConverter = sp.GetRequiredService<SeedBuilder.SeedPriceConverter>();

                var database = sp.GetRequiredService<Database>();

                return new SeedBuilderFactory(database.Plant, descriptionProvider, hardinessZoneSelector, priceConverter);
            });
        }

        private void ConfigureWebDependencies(IServiceCollection services)
        {
            // Authentication
            services.AddIdentity<Vendor, IdentityRole>()
                .AddEntityFrameworkStores<Database>();

            // Presentation
            services.AddControllersWithViews();
            services.AddRazorPages();
        }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Infrastructure Dependencies
            // -------------------------

            // Database
            ConfigureDatabase(services);

            // External Clients
            ConfigureClients(services);

            // Application Dependencies
            // -------------------------

            // Vendor Verification Module
            ConfigureVendorVerification(services);

            // Seed Registration Module
            ConfigureSeedRegistration(services);


            // Web Dependencies
            // -------------------------

            ConfigureWebDependencies(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    "default", "{controller=Home}/{action=Index}/{id?}"
                );
            });

            DbStartSeedData.EnsurePopulated(app);
        }
    }
}
