﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WCIG6.Models;
using WCIG6.ViewModels;

namespace WCIG6.Controllers
{
    public class PlantsController : Controller
    {
        private Database _db;

        public PlantsController( Database db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetPlantData(HomeViewModel viewModel)

        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            viewModel.Plants = _db.Plant.Where(x => x.HardinessZone <= viewModel.HardinessZone)
                .ToList();
            List<Area> AreaList = _db.Area.ToList();
            viewModel.AreaList = AreaList;
            return View( "Index", viewModel);
        }

        [HttpPost]
        public IActionResult GetPlantDataByCity(HomeViewModel viewModel)

        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            int cityZone = _db.Area.FirstOrDefault(x => x.Name.ToLower().Equals(viewModel.CityName.ToLower())).HardinessZone;
            viewModel.HardinessZone = cityZone;
            viewModel.Plants = _db.Plant.Where(x => x.HardinessZone <= viewModel.HardinessZone)
                .ToList();
            List<Area> AreaList = _db.Area.ToList();
            viewModel.AreaList = AreaList;
            return View("Index", viewModel);
        }


        public IActionResult PlantDetails()
        {
            return View();
        }

        public IActionResult SearchSeeds()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SearchSeeds(SeedSearchViewModel model)
        {
            return View();
        }
    }
}
