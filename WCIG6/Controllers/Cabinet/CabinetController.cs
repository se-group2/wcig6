﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WCIG6.Business.SeedRegistration;
using WCIG6.Models;
using WCIG6.ViewModels.Cabinet;

namespace WCIG6.Controllers
{
    public class CabinetController : Controller
    {
        private readonly Database database;
        private readonly SeedBuilderFactory seedBuilderFactory;
        private readonly UserManager<Vendor> userManager;
        private readonly ILogger<CabinetController> logger;

        private Task<Vendor> currentVendor()
        {
            return userManager.GetUserAsync(HttpContext.User);
        }

        private async Task<Seed> findSeed(int seedId, Vendor vendor)
        {
            var seed = await database.Seed
                .Include(seed => seed.Plant)
                .Include(seed => seed.Vendor)
                .SingleOrDefaultAsync(seed => seed.SeedId == seedId && seed.Vendor.Id == vendor.Id);

            return seed;
        }

        private async Task<bool> checkSeed(int seedId, Vendor vendor)
        {
            var count = await database.Seed
                .Include(seed => seed.Vendor)
                .CountAsync(seed => seed.SeedId == seedId && seed.Vendor.Id == vendor.Id);

            return count > 0;
        }

        private async Task changeSeedVisibility(int seedId, bool isVisible)
        {
            var vendor = await currentVendor();
            var seed = await findSeed(seedId, vendor);

            if (seed != null)
            {
                seed.IsVisible = isVisible;

                database.Seed.Update(seed);

                await database.SaveChangesAsync();
            }
        }

        private SeedBuilder buildSeedFrom(SeedViewModel viewModel, Vendor vendor)
        {
            var builder = seedBuilderFactory.forPlant(viewModel.PlantId)
                .withName(viewModel.SeedName)
                .withPrice(viewModel.Price ?? 0)
                .withDescription(viewModel.Description)
                .withVisibility(viewModel.IsPublished)
                .withVendor(vendor);

            if(viewModel.HardinessZone >= 0)
            {
                builder.withHardinessZone(viewModel.HardinessZone);
            }

            return builder;
        }

        private List<Plant> loadPlants()
        {
            return database.Plant.ToList();
        }

        private IActionResult seedFormView(bool isNew, int? seedId, SeedViewModel model)
        {
            ViewBag.IsNew = isNew;

            if(seedId != null)
            {
                ViewBag.SeedId = seedId;
            }

            ViewBag.Plants = loadPlants();

            return View("SeedForm", model);
        }

        public CabinetController(
            Database database,
            SeedBuilderFactory seedBuilderFactory,
            UserManager<Vendor> userManager,
            ILogger<CabinetController> logger
        ) {
            this.database = database;
            this.seedBuilderFactory = seedBuilderFactory;
            this.userManager = userManager;
            this.logger = logger;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            var vendor = await currentVendor();

            var seeds = database.Seed
                .Include(seed => seed.Plant)
                .Where(seed => seed.Vendor.Id == vendor.Id)
                .ToList();

            return View(seeds);
        }

        [Authorize]
        public async Task<IActionResult> PublishSeed(int seedId)
        {
            await changeSeedVisibility(seedId, true);

            return RedirectToAction("Index");
        }

        [Authorize]
        public async Task<IActionResult> UnpublishSeed(int seedId)
        {
            await changeSeedVisibility(seedId, false);

            return RedirectToAction("Index");
        }

        [Authorize]
        public async Task<IActionResult> DeleteSeed(int seedId)
        {
            var vendor = await currentVendor();
            var seed = await findSeed(seedId, vendor);

            if(seed != null)
            {
                database.Seed.Remove(seed);

                await database.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpGet]
        public IActionResult AddSeed()
        {
            return seedFormView(true, null, new SeedViewModel());
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddSeed(SeedViewModel model)
        {
            var vendor = await currentVendor();

            var result = buildSeedFrom(model, vendor).build();

            if(!result.IsSuccessful)
            {
                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error);
                }
            }

            if(!ModelState.IsValid)
            {
                return seedFormView(true, null, model);
            }

            database.Seed.Add(result.Seed);

            database.SaveChanges();

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> EditSeed(int id)
        {
            var vendor = await currentVendor();
            var seed = await findSeed(id, vendor);

            if(seed == null)
            {
                return RedirectToAction("Index");
            }

            return seedFormView(false, id, new SeedViewModel(seed));
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> EditSeed(int id, SeedViewModel model)
        {
            var vendor = await currentVendor();
            var check = await checkSeed(id, vendor);

            if (!check)
            {
                return RedirectToAction("Index");
            }

            var result = buildSeedFrom(model, vendor).build(id);

            if (!result.IsSuccessful)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error);
                }
            }

            if (!ModelState.IsValid)
            {
                return seedFormView(false, id, model);
            }

            database.Seed.Update(result.Seed);

            database.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}
