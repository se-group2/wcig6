﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WCIG6.Models;

namespace WCIG6.ViewModels.Cabinet
{
    // Note: Not using validation - Done inside builder anyway
    public class SeedViewModel
    {
        public SeedViewModel()
        {
            HardinessZone = -1;
        }

        public SeedViewModel(Seed seed)
        {
            SeedName = seed.Name;
            PlantId = seed.Plant.PlantId;
            Price = seed.DecimalPricePerKG;
            HardinessZone = seed.HardinessZone;
            Description = seed.Description;
            IsPublished = seed.IsVisible;
        }

        public string SeedName { get; set; }

        public int PlantId { get; set; }

        public double? Price { get; set; }

        public int HardinessZone { get; set; }

        public string Description { get; set; }

        public bool IsPublished { get; set; }
    }
}
