﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WCIG6.Models;

namespace WCIG6.ViewModels
{
    public class HomeViewModel
    {
        public List<Plant> Plants { get; set; }
        
        public List<Area> AreaList { get; set; }

        public int? HardinessZone { get; set; }

        public string? CityName { get; set; }

    }
}
