﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WCIG6.Models;
using WCIG6.ViewModels;

namespace WCIG6.Controllers
{
    public class HomeController : Controller
    {

        private Database _db;

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, Database db)
        {
            _logger = logger;
            _db = db;

        }

        public IActionResult Index(HomeViewModel viewModel)
        {
            List<Area> AreaList = _db.Area.ToList();
            viewModel.AreaList = AreaList;
            return View(viewModel);
        }

        //[HttpPost]
        //public IActionResult GetPlantData(HomeViewModel viewModel)
        
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    viewModel.Plants = _db.Plant.Where(x => x.HardinessZone <= viewModel.HardinessZone)
        //        .ToList();
        //    List<Area> AreaList = _db.Area.ToList();
        //    viewModel.AreaList = AreaList;
        //    return View("Index", viewModel);
        //}

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult FindPlants()
        {
            return RedirectToAction("Index", "Plants");
        }

    }
}
