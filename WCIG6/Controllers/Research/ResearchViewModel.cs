﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WCIG6.ViewModels
{
    public class ResearchViewModel
    {
        public string DataJson { get; set; }
        public string Location { get; set; }
        public int StartYear { get; set; }
        public int EndYear { get; set; }
        public bool IncludeHardinessZone { get; set; }
        public bool IncludeMinTemp { get; set; }
        public bool IncludeMaxTemp { get; set; }


    }
}
