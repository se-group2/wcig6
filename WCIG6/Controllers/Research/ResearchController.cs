﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using WCIG6.Models;
using WCIG6.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace WCIG6.Controllers
{
    
    public class ResearchController : Controller
    {
        private Database _db;

        public ResearchController(Database database)
        {
            _db = database;
        }

        public IActionResult Index(ResearchViewModel viewModel)
        {
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult GetClimateData(ResearchViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            var climateReports = _db.ClimateReport.Where(x => viewModel.StartYear <= x.Year && x.Year <= viewModel.EndYear && x.Area.Name == viewModel.Location)
                .ToList();

            var options = new JsonSerializerOptions
            {
                WriteIndented = true,
                IgnoreNullValues = true
            };
            viewModel.DataJson = JsonSerializer.Serialize(climateReports,options);

            return View("Index", viewModel);
        }

        [HttpPost]
        public IActionResult GetPlantData(ResearchViewModel viewModel)
        {
            //TODO: Set the viewModel.DataJson to some plant data
            return View("Index", viewModel);
        }
    }
}
