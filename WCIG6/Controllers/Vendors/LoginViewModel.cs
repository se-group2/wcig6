﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WCIG6.ViewModels.Cabinet
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
