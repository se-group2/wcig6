﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WCIG6.Business.VendorVerification;
using WCIG6.Models;
using WCIG6.ViewModels.Cabinet;

namespace WCIG6.Controllers
{
    public class VendorsController : Controller
    {
        private static readonly bool USE_VERIFICATION_CODES = false;

        private readonly VendorVerifier vendorVerifier;
        private readonly UserManager<Vendor> userManager;
        private readonly SignInManager<Vendor> signInManager;
        private readonly ILogger<VendorsController> logger;

        public VendorsController(
            VendorVerifier vendorVerifier,
            UserManager<Vendor> userManager,
            SignInManager<Vendor> signInManager,
            ILogger<VendorsController> logger
        ) {
            this.vendorVerifier = vendorVerifier;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.logger = logger;
        }

        public IActionResult Enter()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Cabinet");
            }

            ViewBag.IsLogin = false;

            return View(new EnterViewModel
            {
                Login = new LoginViewModel(),
                Register = new RegisterViewModel(),
            });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            // Makig sure that user signing in is verified
            var user = await userManager.FindByEmailAsync(model.Email);

            if(user == null || !user.IsVerified)
            {
                ModelState.AddModelError(string.Empty, "Invalid email or password");
            }
            else
            {
                // Can only sign in with username -> Having to find a user with a matching email first
                var result = await signInManager.PasswordSignInAsync(
                    model.Email,
                    model.Password,
                    true,
                    false
                );

                if (!result.Succeeded)
                {
                    ModelState.AddModelError(string.Empty, "Invalid email or password");
                }
            }

            if (!ModelState.IsValid)
            {
                ViewBag.IsLogin = true;

                return View("Enter", new EnterViewModel
                {
                    Login = model,
                    Register = new RegisterViewModel(),
                });
            }

            return RedirectToAction("Index", "Cabinet");
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            var vendor = new Vendor()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.Phone,
                UserName = model.Email,
                Email = model.Email,
                Website = model.Website,
                IsVerified = !USE_VERIFICATION_CODES, // If no codse are used, vendor would automatically be verified
            };

            var verificationResult = vendorVerifier.verify(vendor);

            if (!verificationResult.IsSuccessful)
            {
                foreach (var error in verificationResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error);
                }
            }

            if (ModelState.IsValid)
            {
                var result = await userManager.CreateAsync(vendor, model.Password);

                if (!result.Succeeded)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
                else
                {
                    await signInManager.SignInAsync(vendor, false);
                }
            }

            if (!ModelState.IsValid)
            {
                ViewBag.IsLogin = false;

                return View("Enter", new EnterViewModel
                {
                    Register = model,
                    Login = new LoginViewModel(),
                });
            }

            if(!USE_VERIFICATION_CODES)
            {
                return RedirectToAction("Index", "Cabinet");
            }
            else
            {
                vendorVerifier.sendCodes(vendor);

                return View("Verify");
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Exit()
        {
            await signInManager.SignOutAsync();

            return RedirectToAction("Enter");
        }
    }
}
