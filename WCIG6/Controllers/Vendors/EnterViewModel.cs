﻿using System;
namespace WCIG6.ViewModels.Cabinet
{
    public class EnterViewModel
    {
        public LoginViewModel Login { get; set; }
        public RegisterViewModel Register { get; set; }
    }
}
