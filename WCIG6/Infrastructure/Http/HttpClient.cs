﻿using System;
namespace WCIG6.Infrastructure.Http
{
    public interface HttpClient
    {
        bool testURL(string url);
    }
}
