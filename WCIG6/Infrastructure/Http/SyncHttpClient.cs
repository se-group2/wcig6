﻿using System;
using System.Net.Http;
using Microsoft.Extensions.Logging;

namespace WCIG6.Infrastructure.Http
{
    public class SyncHttpClient : HttpClient
    {
        private readonly IHttpClientFactory clientFactory;

        public SyncHttpClient(IHttpClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
        }

        public bool testURL(string url)
        {
            var client = clientFactory.CreateClient();

            var request = new HttpRequestMessage(HttpMethod.Get,url);

            try
            {
                var response = client.SendAsync(request).Result;

                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }
    }
}
