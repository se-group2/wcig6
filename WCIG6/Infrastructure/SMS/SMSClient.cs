﻿using System;
using System.Threading.Tasks;

namespace WCIG6.Infrastructure.SMS
{
    public interface SMSClient
    {
        void send(string phone, string message);
    }
}
