﻿using System;
namespace WCIG6.Infrastructure.Mail
{
    public interface MailClient
    {
        void send(string email, string message);
    }
}
