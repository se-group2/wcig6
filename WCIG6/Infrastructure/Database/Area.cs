﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WCIG6.Models
{
    public class Area
    {
        public int AreaId{ get; set; }
        public string Name { get; set; }

        public int HardinessZone { get; set; }
        public double? Lattitude { get; set; } 
        public double? Longitude { get; set; }

        public ICollection<ClimateReport> ClimateReports { get; set; }

    }
}
