﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace WCIG6.Models
{
    public class Database : IdentityDbContext<Vendor>
    {
        public Database(DbContextOptions<Database> options)
            : base(options)
        {
        }

        public DbSet<Area> Area { get; set; }
        public DbSet<ClimateReport> ClimateReport { get; set; }
        public DbSet<Plant> Plant { get; set; }
        public DbSet<Seed> Seed { get; set; }
        public DbSet<Vendor> Vendor { get; set; }
        public DbSet<VerificationCode> Code { get; set; }
    }
}
