﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WCIG6.Models
{
    public class DbStartSeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            Database context = app.ApplicationServices
                .CreateScope().ServiceProvider.GetRequiredService<Database>();

            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }

            List<Seed> seeds = new List<Seed>();

            if (!context.Area.Any())
            {
                List<Area> areas = new List<Area> {
                    new Area
                    {
                        Name = "Toronto",
                        HardinessZone = 5,
                        Lattitude = 43.7,
                        Longitude = -79.416
                    },
                    new Area
                    {
                        Name = "Montreal",
                        HardinessZone = 6,
                        Lattitude = 45.509,
                        Longitude = -73.588
                    },
                    new Area
                    {
                        Name = "Ottawa",
                        HardinessZone = 5,
                        Lattitude = 45.411,
                        Longitude = -75.698
                    },
                    new Area
                    {
                        Name = "Calgary",
                        HardinessZone = 4,
                        Lattitude = 51.05,
                        Longitude = -114.085
                    },
                    new Area
                    {
                        Name = "Edmonton",
                        HardinessZone = 3,
                        Lattitude = 53.55,
                        Longitude = -113.469
                    },
                    new Area
                    {
                        Name = "Mississauga",
                        HardinessZone = 6,
                        Lattitude = 43.579,
                        Longitude = -79.658
                    },
                    new Area
                    {
                        Name = "Winnipeg",
                        HardinessZone = 3,
                        Lattitude = 49.884,
                        Longitude = -97.147
                    },
                    new Area
                    {
                        Name = "Vancouver",
                        HardinessZone = 7,
                        Lattitude = 49.25,
                        Longitude = -123.119
                    },
                        new Area
                    {
                        Name = "Quebec",
                        HardinessZone = 5,
                        Lattitude = 46.812,
                        Longitude =  -71.215
                    },
                        new Area
                    {
                        Name = "Hamilton",
                        HardinessZone = 4,
                        Lattitude = 43.25,
                        Longitude =  -79.85
                    },
                        new Area
                    {
                        Name = "Brampton",
                        HardinessZone = 4,
                        Lattitude = 43.683,
                        Longitude = -79.766
                    },
                        new Area
                    {
                        Name = "Surrey",
                        HardinessZone = 8,
                        Lattitude = 49.106,
                        Longitude = -122.825
                    },

                        //For testing high and low hardiness zones by name
                               new Area
                    {                           
                        Name = "High", 
                        HardinessZone = 14,
                        Lattitude = 49.106,
                        Longitude = -122.825
                    },
                           new Area
                    {
                        Name = "Low",
                        HardinessZone = 1,
                        Lattitude = 49.106,
                        Longitude = -122.825
                    }

                };

                foreach (Area area in areas)
                {
                    context.Area.Add(area);
                }
                context.SaveChanges();
            }
            



            if (!context.Plant.Any())
            {
                List<Plant> plants = new List<Plant> {

                    new Plant
                    {
                        Name = "Haemanthus blossom",
                        HardinessZone = 12,
                        PictureURL = "https://alchetron.com/cdn/haemanthus-coccineus-34d62ec5-f876-40d8-b084-4c5bc9e5b80-resize-750.jpeg",
                        Description = "Haemanthus is a Southern African genus of flowering plants in the family Amaryllidaceae, subfamily Amaryllidoideae.[1] Members of the genus are known as blood lily and paintbrush lily. There are some 22 known species, native to South Africa, Botswana, Namibia, Lesotho and Swaziland. About 15 species occur in the winter rainfall region of Namaqualand and the Western Cape, the remainder being found in the summer rainfall region, with one species Haemanthus albiflos occurring in both regions.",
                        SunLevel = "Partial Shade",
                        Edible = false
                    },
                    new Plant
                    {
                        Name = "Tomatoes",
                        HardinessZone = 10,
                        PictureURL = "https://images-prod.healthline.com/hlcmsresource/images/AN_images/tomatoes-1296x728-feature.jpg",
                        Description = "Tomato, (Solanum lycopersicum), flowering plant of the nightshade family (Solanaceae), cultivated extensively for its edible fruits. Labelled as a vegetable for nutritional purposes, tomatoes are a good source of vitamin C and the phytochemical lycopene. The fruits are commonly eaten raw in salads, served as a cooked vegetable, used as an ingredient of various prepared dishes, and pickled. Additionally, a large percentage of the world’s tomato crop is used for processing; products include canned tomatoes, tomato juice, ketchup, puree, paste, and “sun-dried” tomatoes or dehydrated pul",
                        SunLevel = "Full Sun",
                        Edible = true
                    },
                     new Plant
                    {
                        Name = "Cucumber",
                        HardinessZone = 10,
                        PictureURL = "https://www.shethepeople.tv/wp-content/uploads/2019/05/cucumber-e1558166231577.jpg",
                        Description = "The cucumber plant is a tender annual with a rough, succulent, trailing stem. The hairy leaves have three to five pointed lobes, and the stem bears branched tendrils by which the plant can be trained to supports. The five-petaled yellow flowers are unisexual and produce a type of berry known as a pepo",
                        SunLevel = "Full Sun",
                        Edible = true
                    },
                      new Plant
                    {
                        Name = "Chives",
                        HardinessZone = 3,
                        PictureURL = "https://images-na.ssl-images-amazon.com/images/I/51q2Srn0jjL._AC_.jpg",
                        Description = "Chives, scientific name Allium schoenoprasum, is a species of flowering plant in the family Amaryllidaceae that produces edible leaves and flowers. Their close relatives include the common onions, garlic, shallot, leek, scallion, and Chinese onion.",
                        SunLevel = "Full Sun",
                        Edible = true
                    },
                     new Plant
                    {
                        Name = "Asparagus",
                        HardinessZone = 2,
                        PictureURL = "https://cdn.shopify.com/s/files/1/0011/2341/8172/products/AS109-Guelph-Millennium-Asparagus-Seeds-110464655_480x480.jpg?v=1610589124",
                        Description = "Asparagus, or garden asparagus, folk name sparrow grass, scientific name Asparagus officinalis, is a perennial flowering plant species in the genus Asparagus. Its young shoots are used as a spring vegetable. It was once classified in the lily family, like the related Allium species, onions and garlic.",
                        SunLevel = "Full Sun",
                        Edible = true
                    },
                    new Plant
                    {
                        Name = "Mint",
                        HardinessZone = 4,
                        PictureURL = "https://cdn.shopify.com/s/files/1/0011/2341/8172/products/HR1185-Spearmint_480x480.jpg?v=1610656861",
                        Description = "Mentha is a genus of plants in the family Lamiaceae. The exact distinction between species is unclear; it is estimated that 13 to 24 species exist. Hybridization occurs naturally where some species range overlap. Many hybrids and cultivars are known.",
                        SunLevel = "Partial Shade",
                        Edible = true
                    },
                    new Plant
                    {
                        Name = "Basil",
                        HardinessZone = 3,
                        PictureURL = "https://cdn.shopify.com/s/files/1/0011/2341/8172/products/HR1025_Basil-Dolly_1_480x480.jpg?v=1610656231",
                        Description = "Basil, also called great basil, is a culinary herb of the family Lamiaceae. Basil is native to tropical regions from central Africa to Southeast Asia. It is a tender plant, and is used in cuisines worldwide.",
                        SunLevel = "Full Sun",
                        Edible = true
                    },
                    new Plant
                    {
                        Name = "Tulip",
                        HardinessZone = 3,
                        PictureURL = "https://cdn.shopify.com/s/files/1/0011/2341/8172/products/BU472-Tulip-Apricona_480x480.jpg?v=1610571620",
                        Description = "Tulips form a genus of spring-blooming perennial herbaceous bulbiferous geophytes. The flowers are usually large, showy and brightly colored, generally red, pink, yellow, or white. They often have a different colored blotch at the base of the tepals, internally.",
                        SunLevel = "Full Sun",
                        Edible = false
                    },
                    new Plant
                    {
                        Name = "Sunflower",
                        HardinessZone = 1,
                        PictureURL = "https://www.gardenia.net/storage/app/public/uploads/images/detail/shutterstock_1260151384Optimized.jpg",
                        Description = "Helianthus (/ˌhiːliˈænθəs/)[3] is a genus comprising about 70 species of annual and perennial flowering plants in the daisy family Asteraceae.[4][5] Except for three South American species, the species of Helianthus are native to North America and Central America. The common names 'sunflower' and 'common sunflower' typically refer to the popular annual species Helianthus annuus, whose round flower heads in combination with the ligules look like the sun.[6] This and other species, notably Jerusalem artichoke (H. tuberosus), are cultivated in temperate regions and some tropical regions as food crops for humans, cattle, and poultry, and as ornamental plants.[7] The species H. annuus typically grows during the summer and into early fall, with the peak growth season being mid-summer.[8]",
                        SunLevel = "Full Sun",
                        Edible = true
                    },
                };
                foreach (Plant plant in plants)
                {
                    context.Plant.Add(plant);
                }
                context.SaveChanges();
            }


            if (!context.ClimateReport.Any())
            {
                List<ClimateReport> climateReports = new List<ClimateReport>
                {
                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -19.0,
                        MaxTemp = 36.0,
                        Area = context.Area.Where(a => a.Name == "Toronto").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -22.0,
                        MaxTemp = 34.0,
                        Area = context.Area.Where(a => a.Name == "Toronto").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -22.0,
                        MaxTemp = 33.0,
                        Area = context.Area.Where(a => a.Name == "Toronto").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -24.0,
                        MaxTemp = 37.0,
                        Area = context.Area.Where(a => a.Name == "Montreal").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -23.0,
                        MaxTemp = 33.0,
                        Area = context.Area.Where(a => a.Name == "Montreal").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -27.0,
                        MaxTemp = 36.0,
                        Area = context.Area.Where(a => a.Name == "Montreal").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -26.0,
                        MaxTemp = 37.0,
                        Area = context.Area.Where(a => a.Name == "Ottawa").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -27.0,
                        MaxTemp = 33.0,
                        Area = context.Area.Where(a => a.Name == "Ottawa").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -28.0,
                        MaxTemp = 36.0,
                        Area = context.Area.Where(a => a.Name == "Ottawa").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -32.0,
                        MaxTemp = 33.0,
                        Area = context.Area.Where(a => a.Name == "Calgary").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -31.0,
                        MaxTemp = 32.0,
                        Area = context.Area.Where(a => a.Name == "Calgary").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -21.0,
                        MaxTemp = 37.0,
                        Area = context.Area.Where(a => a.Name == "Calgary").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -42.0,
                        MaxTemp = 31.0,
                        Area = context.Area.Where(a => a.Name == "Edmonton").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -41.0,
                        MaxTemp = 31.0,
                        Area = context.Area.Where(a => a.Name == "Edmonton").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -39.0,
                        MaxTemp = 32.0,
                        Area = context.Area.Where(a => a.Name == "Edmonton").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -20.6,
                        MaxTemp = 35.5,
                        Area = context.Area.Where(a => a.Name == "Mississauga").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -22.8,
                        MaxTemp = 31.8,
                        Area = context.Area.Where(a => a.Name == "Mississauga").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -23.5,
                        MaxTemp = 35.4,
                        Area = context.Area.Where(a => a.Name == "Mississauga").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -33.5,
                        MaxTemp = 33.4,
                        Area = context.Area.Where(a => a.Name == "Winnipeg").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -40.0,
                        MaxTemp = 36.1,
                        Area = context.Area.Where(a => a.Name == "Winnipeg").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -31.1,
                        MaxTemp = 37.5,
                        Area = context.Area.Where(a => a.Name == "Winnipeg").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -6.2,
                        MaxTemp = 29.4,
                        Area = context.Area.Where(a => a.Name == "Vancouver").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -7.2,
                        MaxTemp = 30.5,
                        Area = context.Area.Where(a => a.Name == "Vancouver").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -5.5,
                        MaxTemp = 32.4,
                        Area = context.Area.Where(a => a.Name == "Vancouver").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -31.2,
                        MaxTemp = 33.8,
                        Area = context.Area.Where(a => a.Name == "Quebec").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -29.6,
                        MaxTemp = 31.8,
                        Area = context.Area.Where(a => a.Name == "Quebec").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -34.4,
                        MaxTemp = 32.8,
                        Area = context.Area.Where(a => a.Name == "Quebec").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -19.1,
                        MaxTemp = 34.8,
                        Area = context.Area.Where(a => a.Name == "Hamilton").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -24.4,
                        MaxTemp = 32.4,
                        Area = context.Area.Where(a => a.Name == "Hamilton").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -24.5,
                        MaxTemp = 32.7,
                        Area = context.Area.Where(a => a.Name == "Hamilton").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -20.6,
                        MaxTemp = 35.5,
                        Area = context.Area.Where(a => a.Name == "Brampton").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -22.8,
                        MaxTemp = 33.0,
                        Area = context.Area.Where(a => a.Name == "Brampton").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -23.5,
                        MaxTemp = 35.4,
                        Area = context.Area.Where(a => a.Name == "Brampton").FirstOrDefault()
                    },

                    new ClimateReport
                    {
                        Year = 2020,
                        MinTemp = -8.0,
                        MaxTemp = 29.3,
                        Area = context.Area.Where(a => a.Name == "Surrey").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2019,
                        MinTemp = -8.8,
                        MaxTemp = 29.9,
                        Area = context.Area.Where(a => a.Name == "Surrey").FirstOrDefault()
                    },
                    new ClimateReport
                    {
                        Year = 2018,
                        MinTemp = -7.5,
                        MaxTemp = 29.0,
                        Area = context.Area.Where(a => a.Name == "Surrey").FirstOrDefault()
                    },
                };

                foreach (ClimateReport cr in climateReports)
                {
                    context.ClimateReport.Add(cr);
                }
                context.SaveChanges();
            }
        }
    }
}
