﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace WCIG6.Models
{
    public class Vendor: IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Website { get; set; }

        public bool IsVerified { get; set; }
        public ICollection<VerificationCode> Codes { get; set; }

        public string FullName => FirstName + " " + LastName;

        public override string ToString()
        {
            return FullName;
        }
    }
}
