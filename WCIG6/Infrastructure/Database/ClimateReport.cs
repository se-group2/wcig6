﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WCIG6.Models
{
    public class ClimateReport
    {
        public int ClimateReportId{ get; set; }
        public int Year { get; set; }
        public double MinTemp { get; set; }
        public double MaxTemp { get; set; }
        public Area Area { get; set; }

    }
}
