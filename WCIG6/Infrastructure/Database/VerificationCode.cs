﻿using System;
namespace WCIG6.Models
{
    public class VerificationCode
    {
        public int VerificationCodeId { get; set; }

        public string Code { get; set; }

        public Vendor Vendor { get; set; }
    }
}
