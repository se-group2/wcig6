﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WCIG6.Models
{
    public class Plant
    {
        public int PlantId{ get; set; }
        public string Name { get; set; }
        public int HardinessZone { get; set; }
        public string PictureURL { get; set; }
        public string Description { get; set; }
        public string SunLevel { get; set; }
        public bool Edible { get; set; }
    }
}
