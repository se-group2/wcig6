﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WCIG6.Models
{
    public class Seed
    {
        public int SeedId { get; set; }
        public string Name { get; set; }
        public Plant Plant { get; set; }
        public int PricePerKG { get; set; }
        public int HardinessZone { get; set; }
        public string Description { get; set; }
        public Vendor Vendor { get; set; }
        public bool IsVisible { get; set; }

        public double DecimalPricePerKG => ((double)PricePerKG) / 100;
        public string FormattedPricePerKG => DecimalPricePerKG + "$";
    }
}
